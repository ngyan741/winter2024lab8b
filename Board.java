//import random
import java.util.Random;
public class Board{
	//board game grid represented by a Tile 2D array
	private Tile[][] grid;
	//variable that represents the size of the board
	private final int boardSize = 5;
	//Board constructor that intializes the grid array and fills it with blank tiles, then makes one random tile in each row a wall tile.
	public Board(){
		Random rng = new Random();
		grid = new Tile[boardSize][boardSize];
		for(int i=0; i<grid.length; i++){
			int randIndex = rng.nextInt(grid[i].length);
			for(int j=0; j<grid[i].length; j++){
				grid[i][j] = Tile.BLANK;
			}
			grid[i][randIndex] = Tile.HIDDEN_WALL;
		}
		
	}
	//overriding toString method so that the board will be in the shape of a 5 x 5 grid when printed
	public String toString(){
		String output = "";
		for(int i=0; i<this.grid.length; i++){
			for(int j=0; j<this.grid[i].length; j++){
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	//placeToken method that places a token on the board that takes in the row and column of the tile. It returns -2 if the row and column are not within the board's space. Returns -1 if that tile is a CASTLE or WALL tile. Returns 1 if that tile is a HIDDEN WALL and turn it into a WALL tile. Return 0 if the tile is BLANK and place down a CASTLE tile there.
	public int placeToken(int row, int col){
		if(row < 0 || row > boardSize-1 || col < 0 || col > boardSize-1){
			return -2;
		}
		else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL){
			return -1;
		}
		else if(this.grid[row][col] == Tile.HIDDEN_WALL){
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		else{
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
	
	

}