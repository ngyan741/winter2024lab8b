//import Scanner
import java.util.Scanner;
public class BoardGameApp{
	public static void main (String[] args){
		Scanner reader = new Scanner(System.in);
		//greet the user
		System.out.println("Welcome, player, to the Board Game!");
		//create a new board object for the game
		Board gameBoard = new Board();
		//int variable that represents the amount of castles the user has
		int numCastles = 7;
		//int variable that represents the amount of turns
		int turns = 0;
		//main game loop that runs while numCastles is greater than 0 and while the number of turns is less than 8
		while(numCastles > 0 && turns < 8){
			//print the current gameBoard and the number of castles and turns remaining
			System.out.println(gameBoard);
			System.out.println("Castles remaining: " + numCastles);
			System.out.println("Turns remaining: " + turns);
			//ask the player where to place the token and take those as inputs
			System.out.println("Which row do you want to place your tile?");
			int rowPlacement = Integer.parseInt(reader.nextLine());
			System.out.println("Which column do you want to place your tile?");
			int columnPlacement = Integer.parseInt(reader.nextLine());
			//int variable that represents the output of placeToken using those inputs
			int placeTokenOutput = gameBoard.placeToken(rowPlacement, columnPlacement);
			//while placeToken returns a negative number, that is either the the row and column are not within the board's space or that tile is a CASTLE or WALL tile, tell the player that it is invalid and ask where to place the token again
			while(placeTokenOutput < 0){
				System.out.println("Invalid location, please reenter row and column values:");
				System.out.println("Which row do you want to place your tile?");
				rowPlacement = Integer.parseInt(reader.nextLine());
				System.out.println("Which column do you want to place your tile?");
				columnPlacement = Integer.parseInt(reader.nextLine());
				placeTokenOutput = gameBoard.placeToken(rowPlacement, columnPlacement);
			}
			//if placeToken returns 1, that is that there was a wall at the location, tell the user.
			if(placeTokenOutput == 1){
				System.out.println("There was a wall at that location.");
			}
			//else tell the user that the castle was successfully placed and reduce numCastles by 1
			else{
				System.out.println("Castle successfully placed!");
				numCastles--;
			}
			//increment the number of turns by 1
			turns++;
		}
		//print the final gameBoard, number of castles, and turns
		System.out.println(gameBoard);
		System.out.println("Castles remaining: " + numCastles);
			System.out.println("Turns remaining: " + turns);
		//if the user has no more castles left to place, congratulate the user on winning
		if(numCastles == 0){
			System.out.println("Congratulations! You Won.");
		}
		//else tell them they lost the game
		else{
			System.out.println("You lost... Try again?");
		}
	}
}