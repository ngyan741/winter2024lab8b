public enum Tile{
	//Tile enum values
	BLANK("_"),
	WALL("\u001B[31m" + "W" + "\u001B[0m"),
	HIDDEN_WALL("_"),
	CASTLE("\u001B[36m" + "C" + "\u001B[0m");
	
	//enum's name field
	private final String name;
	//Tile enum constructor
	private Tile(String name){
		this.name = name;
	}
	//getter
	public String getName(){
		return this.name;
	}
}

